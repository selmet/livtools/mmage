## Description

The R package mmage is a tool for simulating dynamics of animal populations by discrete-time matrix models (Caswell, 2001) splitting the populations by sex and age class, referred to as “age-structured” population models. mmage has been designed for simulating tropical livestock populations in which animals are extensively managed.

## Installation

The easiest way is to download [here](https://gitlab.cirad.fr/selmet/livtools/mmage/-/releases) the latest version of the source package from the release section of this repository.

Maybe you prefer install the package mmage directly from the remote repository gitlab, you need to ensure that you have Git software installed on your system. More details [here](https://docs.posit.co/ide/user/ide/guide/tools/version-control.html) on how to install the versioning tool Git in [RStudio](https://posit.co/download/rstudio-desktop/)
Then you use install_git from the package [remotes.](https://CRAN.R-project.org/package=remotes) 

```R
install.packages("remotes")
library(remotes)
```

for the current developing version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/livtools/mmage')
```
or for a specific version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/livtools/mmage', ref = "2.4.2")
```

## Documentation

The mmage manual presents simple illustrations of models representing tropical cattle and sheep populations is available [here](https://gitlab.cirad.fr/selmet/livtools/mmage/-/tree/master/doc) 

## Bibliography

H. Caswell, Matrix population models: construction, analysis and interpretation, 2nd ed. Sunderland, MS, USA: Sinauer Associates, 2001.

## Support

Please send a message to the maintainer: Julia Vuattoux <julia.vuattoux@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)
