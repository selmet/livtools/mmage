\name{mmage-package}
\alias{mmage-package}
\alias{mmage}
\docType{package}
\encoding{latin1}

\title{A R package for age-structured population matrix models}
\description{A set of functions for simulating \bold{age-structured matrix models} for animal, vegetal or human populations
 (http://livtools.cirad.fr)}

\details{


\bold{Index:}
  \preformatted{

\bold{Model structure and parameters}

fclass			Define the age classes of the model.
fhh2vh			Transform hazard rates from "age-to-age" (horizontal rates) to "age class-to-age class" (vertical rates).
fg          Calculates probabilities g using the steady-state approximation. 
fvh2par  		Build the final parameters table from "age class-to-age class" hazard rates.
fvp2par    	Build the final parameters table from "age class-to-age class" probabilities and (for reproduction) hazard rates.

\bold{Matrix building and calculations}

fmatfec  		Build matrix F.
fmatfec_f  	Build matrix F (for female models).
fmatg				Build matrix G.
fmatg_f			Build matrix G (for female models).
fmat				Build matrices F, D, O, I, G and A from the input parameters. 
fmat_f			Build matrices F, D, O, I, G and A from the input parameters (for female models). 

fseasmat    Build matrices for seasonal models.

feig				Calculate the eigenvectors and eigenvalues of matrix A.

\bold{Projection}

fproj				Run the projection.
fproj_diff  Run the projection (for constraints on the population vector with the difference method).
fproj_f			Run the projection (for female models).

\bold{Outputs summary}

fproj2vec		Expand projection outputs to vectors for facilitating their summary.
fm  				Calculate multiplication rates m(t) for a projection.
fprod				Calculate productions (other than milk) for a projection.
fmilk  			Calculate milk productions for a projection.

fsrc        Calculate standardized regression coefficients (SRC) for sensitivity/uncertainty analysis

\bold{Auxiliary functions}

fnorm				Normalize a vector to sum to 1.
frecod_age  Recode age classes.
frecod_phase  Recode cycles and phases.

zcal				Build the projection calendar.
zfm         Prepare the data to calculate multiplication rates.
zhh2vh	    Used by fhh2vhf.
zmat2vec		Expand a matrix to a vector and replicate auxiliary information.
zreptab			Replicate a table.

    }
    
}

\author{
Matthieu Lesnoff \email{matthieu.lesnoff@cirad.fr},
CIRAD, Montpellier, France
}

\keyword{package}

