#---------------- If direct source of the functions
#rm(list = ls())
source("D:/Users/Applications/Tools/sourcedir.R")
path <- "D:/Users/Applications/Tools/Demography/Models/Mmage/Package/Pack_mmage/R"
sourcedir(path, FALSE)
#----------------- End

#---------------- If use of the package
#require(mmage)
#----------------- End

require(ggplot2)

lclassf <- c(1, 3, 10, 1)
lclassm <- c(1, 3, 4, 1)
tcla <- fclass(lclassf, lclassm)
tcla

# normal year
part <- 0.50
netpro <- 1
pfbirth <- 0.50
dea1 <- 0.20 ; dea2 <- 0.07 ; dea3 <- 0.02
offf1 <- 0.00 ; offf2 <- 0.01 ; offf3 <- 0.03
offm1 <- 0.05 ; offm2 <- 0.20 ; offm3 <- 0.45
netpro <- c(0, 0, netpro, netpro, 0, 0, 0, 0) 
pfbirth <- c(0, 0, pfbirth, pfbirth, 0, 0, 0, 0) 

z <- tcla[tcla$class > 0, ]
z$hpar <- c(0, 0, part, part, 0, 0, 0, 0) 
z$hfecf <- pfbirth * netpro * z$hpar
z$hfecm <- (1 - pfbirth) * netpro * z$hpar
z$hdea <- c(dea1, dea2, dea3, dea3, dea1, dea2, dea3, dea3) 
z$hoff <- c(offf1, offf2, offf3, offf3, offm1, offm2, offm3, offm3) 
hh1 <- z
hh1

# drought year
part <- 0.20
netpro <- 1
pfbirth <- 0.50
dea1 <- 0.25 ; dea2 <- 0.15 ; dea3 <- 0.15
offf1 <- 0.00 ; offf2 <- 0.05 ; offf3 <- 0.10
offm1 <- 0.20 ; offm2 <- 0.30 ; offm3 <- 0.80
netpro <- c(0, 0, netpro, netpro, 0, 0, 0, 0) 
pfbirth <- c(0, 0, pfbirth, pfbirth, 0, 0, 0, 0) 

z <- tcla[tcla$class > 0, ]
z$hpar <- c(0, 0, part, part, 0, 0, 0, 0) 
z$hfecf <- pfbirth * netpro * z$hpar
z$hfecm <- (1 - pfbirth) * netpro * z$hpar
z$hdea <- c(dea1, dea2, dea3, dea3, dea1, dea2, dea3, dea3) 
z$hoff <- c(offf1, offf2, offf3, offf3, offm1, offm2, offm3, offm3) 
hh2 <- z
hh2

vh <- fhh2vh(hh1)
param1 <- fvh2par(vh)
param1

vh <- fhh2vh(hh2)
param2 <- fvh2par(vh)
param2

#------ xini

Lf <- Lm <- 4
mat <- fmat(param1, Lf, Lm)
res <- feig(mat$A)
res$lambda
tabini <- tcla[tcla$class > 0, ]
tabini$x <- res$v
tabini
aggregate(formula = x ~ sex, data = tabini, FUN = sum)
sum(tabini$x)

xini <- 1000 * tabini$x
xini

#------------------- Deterministic projection

### Running and describing the projection

param <- list(param1, param2) # two environments

nbcycle <- 20 ; nbphase <- 1
nbstep <- nbcycle * nbphase
env <- rep(1, nbstep)
env[c(4, 5)] <- 2
env # time series of the environments

listpar <- vector("list", length = nbstep)
for(i in 1:nbstep) listpar[[i]] <- param[[env[i]]]
      
proj <- fproj(listpar, xini, nbcycle, nbphase)
names(proj)

proj$matx

vecx <- proj$vecx
vecx
vecprod <- proj$vecprod
vecprod

z <- vecx
z <- aggregate(formula = x ~ tim, data = z, FUN = sum)
z
p <- ggplot(data = z, aes(x = tim, y = x))
p <- p + geom_line() + geom_point()
p <- p + labs(x = "Time (year)", y = "Nb. animals")
p

fm(formula = ~ cycle, vecprod)

res <- fm(formula = ~ cycle + sex, vecprod)
p <- ggplot(data = res, aes(x = cycle, y = m, group = sex))
p <- p + geom_line(aes(colour = sex)) + geom_point(aes(colour = sex))
p <- p + labs(x = "Year", y = "Rate m")
p


#------------------- Stochastic projection

### A single projection

param <- list(param1, param2)

nbcycle <- 20 ; nbphase <- 1
nbstep <- nbcycle * nbphase
theta <- 2 / 10
env <- sample(1:2, size = nbstep, replace = TRUE, prob = c(1 - theta, theta))
env
listpar <- vector("list", length = nbstep)
for(i in 1:nbstep) listpar[[i]] <- param[[env[i]]]

proj <- fproj(listpar, xini, nbcycle, nbphase)
z <- aggregate(formula = x ~ tim, data = proj$vecx, FUN = sum)
head(z)

p <- ggplot(data = z, aes(x = tim, y = x))
p <- p + geom_line() + geom_point()
p <- p + labs(x = "Time (year)", y = "Nb. animals")
p

### Replications of projections

param <- list(param1, param2)

nbcycle <- 20 ; nbphase <- 1
nbstep <- nbcycle * nbphase

nbrep <- 5
theta <- 2 / 10
listpar <- vector("list", length = nbstep)
res <- matrix(nrow = nbstep + 1, ncol = nbrep)

for(i in 1:nbrep) {
  env <- sample(1:2, size = nbstep, replace = TRUE, prob = c(1 - theta, theta))
  for(j in 1:nbstep) listpar[[j]] <- param[[env[j]]]
  proj <- fproj(listpar, xini, nbcycle, nbphase)
  z <- aggregate(formula = x ~ tim, data = proj$vecx, FUN = sum)
  res[, i] <- z$x
}

z <- data.frame(
  tim = rep(0:nbstep, nbrep),
  sim = sort(rep(1:nbrep, nbstep + 1)),
  x = as.vector(res)
)
p <- ggplot(data = z, aes(x = tim, y = x, group = sim))
p <- p + geom_line(aes(colour = as.factor(sim)))
p <- p + labs(x = "Time (year)", y = "Nb. animals")
p










