feig <-
function(A, left = FALSE){

  res <- eigen(A)
  
  # eigenvalues
  values <- res$values 
  
  # V: colums are the right eigenvectors
  V <- res$vectors
  if(all(Re(V[, 1]) <= 0)) V[, 1] <- -V[, 1]
  V[, 1] <- V[, 1] / sum(V[, 1]) # normalization of v1
  V
  
  if(qr(V)$rank < nrow(V)) V.singular <- TRUE else V.singular <- FALSE
  
  # U: rows are the left eigenvectors
  U <- matrix(NA)
  if(!V.singular & left) U <- qr.solve(V) # constraint U %*% V = I
  
  lambda <- Re(values[1])
  v <- Re(V[, 1])
  u <- Re(U[1, ])
  #u <- u / u[1]

list(values = values, lambda = lambda, V = V, U = U, V.singular = V.singular, v = v, u = u)

}
