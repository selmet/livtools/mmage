fhh2vh <-
function(tab) {

  u <- tab[tab$sex == "F", ]
  lclassf <- u$lclass
  tcla <- fclass(lclassf)
  tcla <- tcla[, c("sex", "class", "lclass", "cellmin", "cellmax")]
  
  z <- tcla
  z$hpar <- zhh2vh(u$hpar, lclassf)$h
  z$hfecf <- zhh2vh(u$hfecf, lclassf)$h
  z$hfecm <- zhh2vh(u$hfecm, lclassf)$h
  z$hdea <- zhh2vh(u$hdea, lclassf)$h
  z$hoff <- zhh2vh(u$hoff, lclassf)$h
  
  res <- z
  
  Lm <- length(tab$sex[tab$sex == "M"])  
  if(Lm > 0) {
    u <- tab[tab$sex == "M", ]
    lclassm <- u$lclass
    tcla <- fclass(lclassf, lclassm)
    tcla <- tcla[, c("sex", "class", "lclass", "cellmin", "cellmax")]
    
    z <- tcla[tcla$sex == "M", ]
    z$hpar <- zhh2vh(u$hpar, lclassm)$h
    z$hfecf <- zhh2vh(u$hfecf, lclassm)$h
    z$hfecm <- zhh2vh(u$hfecm, lclassm)$h
    z$hdea <- zhh2vh(u$hdea, lclassm)$h
    z$hoff <- zhh2vh(u$hoff, lclassm)$h
    res <- rbind(res, z)
  }
  
  res
	
}
