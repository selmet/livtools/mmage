fmatfec_f <-
function(param, Lf) {

  fecf <- param$ff[2:(Lf + 1)]

  X <- rbind(fecf, diag(1, Lf))
  
  X

}


